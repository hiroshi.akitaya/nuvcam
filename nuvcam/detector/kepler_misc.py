#
# Miscellaneous tools for Kepler KL400 images.
#
# 2024/05/23 Correct a bug in  modify_fits_header();
#               calculating exptime (x10 error in calculation)

__author__ = 'Hiroshi Akitaya'
__version__ = '1.0.3'

__date__ = '2024/05/23'

import os
import shutil
import sys

from astropy.io import fits
from astropy.io.fits import HDUList

MEASURED_GAIN_TABLE = {
    "ROLLING LDR": {1.85: 0.00},
    "ROLLING HDR": {1.85: 0.00}
}

PIX_SCALE = 0.143  # Pixel scale at Kanata 2024-03.

def rename_file_zfill(fn: str, n_digit=5, fn_only=False, verbose=False) -> str:
    """Rename a file with a name with zero-filled number.
    Example: xxxx_01.fits -> xxxx_00001.fits
    :param fn: input file name
    :n_digit: number of zero-filled digits
    :fn_only: only return file name (not rename the input file)
    :return fn: new file name
    """
    import re
    # Filename string analysis.
    pattern = re.compile(r'(.*_)(\d+)(\.fits*)')
    found = pattern.findall(fn)
    if len(found) == 0:
        return None
    elif len(found[0]) != 3:
        return None

    fn_aligned = found[0][0] + found[0][1].zfill(n_digit) + found[0][2]

    # Return only a file name (optional).
    if fn_only:
        return fn_aligned

    # Rename a file.
    try:
        shutil.move(fn, fn_aligned)
    except IOError:
        sys.stderr.write('File rename error.\n')
        return None
    with fits.open(fn_aligned, 'update') as hdul:
        hdul[0].header['FN_ORG'] = (fn, 'Original file name by FLIPilot.')
        hdul[0].header['HISTORY'] = 'File name was modified by filename_digit_alignment():'
        hdul[0].header['HISTORY'] = f'from {fn} to {fn_aligned}.'
        hdul.flush()
        if verbose:
            print('File renamed successfully. {} -> {}'.format(fn, fn_aligned))
    return fn_aligned


def modify_fits_header(fn, force=True, verbose=False):
    """Modify a FITS file for the NUV camera reduction.
    """
    try:
        hdul = fits.open(fn, 'update')
    except IOError:
        sys.stderr.write(f'File open error: {fn}.\n')
        return None
    hdr = hdul[0].header

    if ('NUVMDFFH' in hdr) and (force is False):
        print('Fits header has already been modified. Abort.')
        return None
    hdr['NUVMDFFH'] = (True, 'Processed by modify_fits_header in nuvcam package.')

    # Exposure time
    exp_time = hdr['EXPTIME']
    if 'EXPT10US' not in hdr:
        exp_time_10us = int(hdr['EXPTIME'])
        hdr['EXPT10US'] = (exp_time_10us, 'Duration of exposure in 10us periods (EXPTIME in FLIPilot output)')
        exp_time_sec = exp_time_10us / 1e5
        hdr['EXPTIME'] = (exp_time_sec, 'Duration of exposure in seconds')

    # Gain
    if 'GAIN_ORG' not in hdr:
        gain_org = float(hdr['GAIN'])
        hdr['GAIN_ORG'] = (gain_org, 'Gain originally shown in FLIPilot output')
        gain = _get_measured_gain_value(hdr)
        hdr['GAIN'] = (gain, 'Gain value [e-/ADU]')

    # Pixel scale
    hdr['PIXSCALE'] = (PIX_SCALE, 'Pixel scale (arcsec/pix)')

    # History
    hdr['HISTORY'] = 'FITS header modified by modify_fits_header().'
    hdul.flush()
    hdul.close()

    if verbose:
        print('FITS header modified successfully. {}'.format(fn))


def _get_measured_gain_value(hdr):
    gain = hdr['GAIN']
    cam_mode = hdr['CAM-MODE']
    try:
        gain_value = MEASURED_GAIN_TABLE[cam_mode][gain]
    except KeyError:
        gain_value = None
    return gain_value


if __name__ == '__main__':
    # Test code.
    import numpy as np

    fn = 'test_5.fit'
    data = np.arange(100.0).reshape(10, 10)
    hdu = fits.PrimaryHDU(data)
    hdul = fits.HDUList([hdu])
    hdul.writeto(fn, overwrite=True)
    result = rename_file_zfill(fn)
    print(result)
