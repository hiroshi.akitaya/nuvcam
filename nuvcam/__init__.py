from .detector import add_telstatus_info, telstatus, jgem, kepler_misc

__copyright__ = 'Copyright 2021, Nuvcam team, ARC/CIT'
__license__ = 'TBD'
__version__ = '0.2.0'
__author__ = 'Hiroshi Akitaya'
__email__ = 'akitaya.hiroshi@p.chibakoudai.jp'
__url__ = 'https://gitlab.com/hiroshi.akitaya/nuvcam'
