import os
import shutil
import time
from watchdog.observers import Observer
from watchdog.events import PatternMatchingEventHandler

"""
Telstatus logger for obtained fits images.
"""

__author__ = "Hiroshi Akitaya"
__copyright__ = "Hiroshi Akitaya, CIT"
__version__ = "1.0.0"


if __name__ == '__main__':

    # TELStatus_sample file path .
    STAT_FN = './test/TELStatus_sample'
    # STAT_FN = '/home/astr/kanata/'

    # Directory to be watched.
    # DIR_WATCH = './test'
    DIR_WATCH = '/home/astr/obsdata'

    PATTERNS = ['*.fits']


    def copy_telstat(fn_stat):
        if os.path.isfile(STAT_FN):
            print(STAT_FN, fn_stat)
            shutil.copy2(STAT_FN, fn_stat)


    def on_created(event):
        filepath = event.src_path
        filename = os.path.basename(filepath)
        dirname = os.path.dirname(filepath)
        fnparts = os.path.splitext(filename)
        telstat_fn = dirname + '/' + fnparts[0] + '.dat'
        print('{} changed'.format(telstat_fn))
        copy_telstat(telstat_fn)


    event_handler = PatternMatchingEventHandler(PATTERNS)
    event_handler.on_created = on_created

    observer = Observer()
    observer.schedule(event_handler, DIR_WATCH, recursive=True)
    observer.start()
    try:
        while True:
            time.sleep(1)
    except KeyboardInterrupt:
        observer.stop()
    observer.join()
