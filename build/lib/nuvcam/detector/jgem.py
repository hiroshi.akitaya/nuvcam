#
# JGEM module for NUVCAM image
#

__author__ = 'Hiroshi Akitaya'
__version__ = '1.0.0'
__date__ = '2024-04-04'

from astropy.io import fits


class JgemKanata(object):

    def __init__(self):
        pass

    def add_jgem_header(self, fn, force=True):
        with fits.open(fn, 'update') as hdul:
            hdr = hdul[0].heeder

            # Check previously processed or not.
            if ('JGEM-TEL' in hdr) and (force is False):
                print('J-GEM headers already exist. Abort.')
                return None

            filter_name = hdr['FILTER']
            mjd = float(hdr['MJD'])
            exptime = float(hdr['EXPTIME'])
            pixscale = float(hdr['PIXSCALE']) / (60 * 60)
            eventid = self.get_eventid()
            gid = self.get_gid()

            hdr['JGEM-TEL'] = ('KANATA-NUVCAM', 'J-GEM Telescope')
            hdr['JGEM-FIL'] = (filter_name, 'J-GEM Filter')
            hdr['JGEM-MJD'] = (mjd, 'J-GEM MJD')
            hdr['JGEM-EXP'] = (exptime, 'J-GEM Exposure')
            hdr['JGEM-SCL'] = (pixscale, 'J-GEM Pixel Scale (arcsec/deg)')
            hdr['JGEM-EID'] = (eventid, 'J-GEM event id')
            hdr['JGEM-GID'] = (gid, 'J-GEM galaxy id')

            hdr['HISTORY'] = 'J-GEM header appended by add_jgem_header().'

            hdul.flush()

    def get_eventid(self):
        return 'TBD'

    def get_gid(self):
        return 'TBD'


if __name__ == '__main__':
    import sys
    fn = sys.argv[1]
    jgem = JgemKanata()
    jgem.add_jgem_header(fn)
