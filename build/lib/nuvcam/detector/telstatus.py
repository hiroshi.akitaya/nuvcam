#!/usr/bin/env python
#
#   Write Kanata TELstatus information to a fits file.
#
#     Since 2024-03-11 H. Akitaya
#

__author__ = 'H. Akitaya'
__version__ = '0.0.1'

import os
import sys

from astropy.io import fits


class TelStatus(object):
    """TelStatus is a class to handle Kanata TELstatus file and append their information to
    a fits file of the NUV camera."""

    def __init__(self, fn_tel=None, debug=False):
        self.fn_tel = fn_tel
        self.values = []
        self.header_values = {}
        self.debug = debug

    def read_items_from_file(self, debug=False):
        """Read TELstatus information from the TELstatus file and store them in a dictionary."""
        with open(self.fn_tel, 'r') as f:
            for line in f.readlines():
                items = line.strip().split()
                if not items:
                    continue
                if items[0].startswith('#'):
                    continue
                self.values.append(items)
            if self.debug:
                print(self.values)

    def interpret_items_to_fitsheader(self):
        """Convert TELstatus information dictionary to FITS header key, value, and comment sets."""
        for val in self.values:
            if len(val) > 4:
                if (val[0] == 'TelPos_base') and (val[1] == 'RA') and (val[3] == 'Dec'):
                    self.header_values['RA'] = (val[2], 'RA of the target base position')
                    self.header_values['DEC'] = (val[4], 'DEC of the target base position')
                if (val[0] == 'TelPos_current') and (val[1] == 'RA') and (val[3] == 'Dec'):
                    self.header_values['RA-END'] = (val[2], 'RA of tel pos at exposure end (Tel Log)')
                    self.header_values['DEC-END'] = (val[4], 'DEC of tel pos at exposure end (Tel Log)')
            if len(val) > 3:
                if (val[0] == 'TelPos_current') and (val[1] == 'Az') and (val[3] == 'Alt'):
                    self.header_values['AZI-END'] = (float(val[2]), 'Azimuth of tel pos [deg] at exp end (Tel Log)')
                    self.header_values['ALT-END'] = (float(val[4]), 'DEC of tel pos at exposure end (Tel Log)')
                elif (val[0] == 'TelPos_speed') and (val[1] == 'Az') and (val[3] == 'Alt'):
                    self.header_values['VAZI-END'] = (float(val[2]), 'Az speed [arcsec/s] at exp end (Tel Log)')
                    self.header_values['VALT-END'] = (float(val[4]), 'Alt speed [arcsec/s] at exp end (Tel Log)')
                elif (val[0] == 'TelPos_current') and (val[1] == 'Cs') and (val[3] == 'Ns1'):
                    self.header_values['CROT-END'] = (float(val[2]), 'PA of Cs rotator [deg] at exp start (Tel Log)')
                    self.header_values['NROT-END'] = (float(val[4]), 'PA of Ns1 rotator [deg] at exp start (Tel Log)')
                elif (val[0] == 'TelSpd_current') and (val[1] == 'Cs') and (val[3] == 'Ns1'):
                    self.header_values['VCRO-END'] = (float(val[2]), 'Cs Rot speed [arcsec/s] at exp end (Tel Log)')
                    self.header_values['NROT-END'] = (float(val[4]), 'Ns1 Rot spd [arcsec/s] at exp end (Tel Log)')
                elif (val[0] == 'TelPos_offset') and (val[1] == 'Cs') and (val[3] == 'Ns1'):
                    self.header_values['ORA-END'] = (float(val[2]), 'Offset of Cs Rot [deg] at exp end (Tel Log)')
                    self.header_values['ODEC-END'] = (float(val[4]), 'Offset of Ns1 Rot [deg] at exp end (Tel Log)')
            elif (val[0] == 'TelPos_offset') and (val[1] == 'RA') and (val[3] == 'DEC'):
                self.header_values['OCRO-END'] = (float(val[2]), 'Offset of RA [arcsec] at exp start (Tel Log)')
                self.header_values['ONRO-END'] = (float(val[4]), 'Offset of DEC [arcsec] at exp start (Tel Log)')
            if len(val) > 2:
                if (val[0] == 'Ter_mirror') and (val[1] == 'Position'):
                    self.header_values['FOC-POS'] = (val[2], 'Beamed focus position (Tel Log)')
                elif (val[0] == 'Sec_mirror') and (val[1] == 'Focus'):
                    self.header_values['FOC-VAL'] = (float(val[2]), 'Focus value (Tel Log)')
                elif (val[0] == 'Sec_mirrorA') and (val[1] == 'FocusA'):
                    self.header_values['FOCVALA'] = (float(val[2]), '2nd mirror position = FOC-VAL (Tel Log)')
                elif (val[0] == 'Sec_mirrorB') and (val[1] == 'FocusB'):
                    self.header_values['FOCVALB'] = (float(val[2]), 'Correction by displacement Sensor (Tel Log)')
                elif (val[0] == 'Sec_mirrorAB') and (val[1] == 'FocusAB'):
                    self.header_values['FOCVALAB'] = (float(val[2]), 'Corrected pos. = FOCVALA - FOCVALB (Tel Log)')
            if len(val) > 1:
                if val[0] == 'Airmass':
                    self.header_values['AIRMASS'] = (float(val[1]), 'Typical airmass (Tel Log)')
                elif val[0] == 'DomeAzimth':
                    self.header_values['AZI-DOME'] = (float(val[1]), 'Dome azimth angle (Tel Log)')
                elif val[0] == 'MirrorCover':
                    self.header_values['M1-COVER'] = (val[1], 'M1 cover status (Tel Log)')
                elif val[0] == 'DomeSlit':
                    self.header_values['DOMESLIT'] = (val[1], 'Dome slit status (Tel Log)')
                elif val[0] == 'MJD':
                    self.header_values['MJDT-END'] = (val[1], 'MJD nearly at exposure end (Tel Log)')
                elif val[0] == 'JST':
                    self.header_values['JSTT-END'] = (val[1], 'JST nearly at exposure end (Tel Log)')
                elif val[0] == 'LST':
                    self.header_values['LSTT-END'] = (val[1], 'LST nearly at exposure end (Tel Log)')
        if self.debug:
            for (key, val) in self.header_values.items():
                print(key, val)

    def add_fits_header(self, hdul):
        """Write fits header to the file."""
        from datetime import datetime
        for keyword, value in self.header_values.items():
            hdul[0].header[keyword] = value
        hdul[0].header['HISTORY'] = 'TELStatus information appended: {}'.format(
            datetime.utcnow().strftime('%Y-%m-%d %H:%M:%S'))


def update_fits_header_reading_telstatus(fn_fits, ext_telstatus='.dat', verbose=False):
    fn_telstatus = os.path.splitext(fn_fits)[0] +  ext_telstatus
    if not os.path.exists(fn_telstatus):
        raise IOError('File {} does not exist'.format(fn_telstatus))
    ts = TelStatus(fn_telstatus)
    ts.read_items_from_file()
    ts.interpret_items_to_fitsheader()
    try:
        hdul = fits.open(fn_fits, mode='update')
    except IOError:
        raise IOError('File {} does not exist'.format(fn_fits))
    ts.add_fits_header(hdul)
    if verbose:
        print('Updating {} ...'.format(fn_fits))
    hdul.flush()
    hdul.close()
    return fn_fits

if __name__ == '__main__':
    fn_tel = sys.argv[1]
    fn_in = sys.argv[2]
    debug = True
    # if len(sys.argv) > 2:
    #    debug = sys.argv[2]
    #    debug = True if debug else False
    tel_status = TelStatus(fn_tel, debug)
    tel_status.read_items_from_file()
    tel_status.interpret_items_to_fitsheader()

    with fits.open(fn_in, mode='update') as hdul:
        tel_status.add_fits_header(hdul)
        hdul.writeto(fn_in, overwrite=True)
