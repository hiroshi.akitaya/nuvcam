#!/usr/bin/env python

#
# Append TELstatus information to fits header.
#    2024-03-12 H. AKitaya
#
#  Usage: add_telstatus_info.py *.fits


__author__ = 'Hiroshi Akitaya'
__version__ = '0.0.1'

__date__ = '2024-03-12'

import argparse
import os

from astropy.io import fits
from nuvcam.detector.telstatus import TelStatus

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('fn', metavar='FILE', nargs='+', help='input fits file')

    args = parser.parse_args()

    for fn_fits in args.fn:
        if not os.path.isfile(fn_fits):
            raise FileNotFoundError(fn_fits)

        fns = os.path.splitext(fn_fits)
        fn_telstat = fns[0] + '.dat'

        if not os.path.isfile(fn_telstat):
            raise FileNotFoundError(fn_telstat)

        with fits.open(fn_fits) as hdul:
            tel_status = TelStatus(fn_telstat)
            tel_status.read_items_from_file()
            tel_status.interpret_items_to_fitsheader()
            tel_status.add_fits_header(hdul)
            hdul.writeto(fn_fits, overwrite=True)