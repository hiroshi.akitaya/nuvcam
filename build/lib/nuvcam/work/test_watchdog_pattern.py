import os
import time
from watchdog.observers import Observer
from watchdog.events import PatternMatchingEventHandler

if __name__ == '__main__':

    DIR_WATCH = 'test'
    PATTERNS = ['*.txt']

    def on_modified(event):
        filepath = event.src_path
        filename = os.path.basename(filepath)
        print('{} changed'.format(filename))

    event_handler = PatternMatchingEventHandler(PATTERNS)
    event_handler.on_modified = on_modified

    observer = Observer()
    observer.schedule(event_handler, DIR_WATCH, recursive=True)
    observer.start()
    try:
        while True:
            time.sleep(1)
    except KeyboardInterrupt:
        observer.stop()
    observer.join()
