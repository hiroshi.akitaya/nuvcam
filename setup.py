from setuptools import setup

setup(
    name='nuvcam',
    version='0.2.1',
#    packages=[''],
    url='',
    license='TBD',
    author='Hiroshi Akitaya',
    author_email='akitaya@perc.it-chiba.ac.jp',
    description='NUV Camera reduction tool'
)
